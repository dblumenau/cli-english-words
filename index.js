/**
 * @param {any} num -
 * @param {boolean} usNumbers
 * @return {string}
 */
const numberToWords = (num, usNumbers = false) => {
    const onesToEnglish = (num) => {
        switch (num) {
            case 1:
                return "one";
            case 2:
                return "two";
            case 3:
                return "three";
            case 4:
                return "four";
            case 5:
                return "five";
            case 6:
                return "six";
            case 7:
                return "seven";
            case 8:
                return "eight";
            case 9:
                return "nine";
        }
        return "";
    };

    const teensToEnglish = (num) => {
        switch (num) {
            case 10:
                return "ten";
            case 11:
                return "eleven";
            case 12:
                return "twelve";
            case 13:
                return "thirteen";
            case 14:
                return "fourteen";
            case 15:
                return "fifteen";
            case 16:
                return "sixteen";
            case 17:
                return "seventeen";
            case 18:
                return "eighteen";
            case 19:
                return "nineteen";
        }
        return "";
    };

    const tensToEnglish = (num) => {
        switch (num) {
            case 2:
                return "twenty-";
            case 3:
                return "thirty-";
            case 4:
                return "forty-";
            case 5:
                return "fifty-";
            case 6:
                return "sixty-";
            case 7:
                return "seventy-";
            case 8:
                return "eighty-";
            case 9:
                return "ninety-";
        }
        return "";
    };

    const getHundreds = (num, last = false) => {
        const wordArray = [];
        const hundredth = Math.trunc(num / 100);
        if (hundredth) {
            wordArray.push(onesToEnglish(hundredth));
            wordArray.push('hundred');
        }
        const tenth = num % 100;
        if (!tenth) {
            return wordArray;
        }
        if (tenth < 10) {
            if (last && !usNumbers) {
                wordArray.push('and');
            }
            wordArray.push(onesToEnglish(tenth));
            return wordArray;
        }
        if (tenth < 20) {
            if (last && !usNumbers) {
                wordArray.push('and');
            }
            wordArray.push(teensToEnglish(tenth));
            return wordArray;
        }
        if (last && !usNumbers) {
            wordArray.push('and');
        }
        wordArray.push(tensToEnglish(Math.trunc(tenth / 10)));
        const unit = tenth % 10;
        if (unit) {
            wordArray.push(onesToEnglish(unit));
        }
        return wordArray;
    };

    const getEnglishNumbers = (num) => {
        if (num === 0) {
            return ['zero'];
        }
        let finalWords = [];
        let thousandth = Math.trunc(num / 1000);
        let last = false;
        if (num > 100 && num < 1000) {
            last = true;
        }
        if (thousandth) {
            finalWords = finalWords.concat(getHundreds(thousandth));
            finalWords.push('thousand');
            last = true;
        }
        num = num % 1000;
        finalWords = finalWords.concat(getHundreds(num, last));
        return finalWords;
    };

    const validateInput = (num) => {
        num = parseInt(num);
        let result = {
            success: undefined,
            message: undefined
        }
        if (isNaN(num)) {
            result.success = false;
            result.message = 'The input could not be converted to a valid integer.';
        } else if (num < 0 || num > 100000) {
            result.success = false;
            result.message = 'The input must be a number between 0 and 100000.';
        } else {
            result.success = true;
            result.message = num;
            if (usNumbers) {
                result.message = getEnglishNumbers(num).join(' ').replace(/- /g, ' ');
            } else {
                result.message = getEnglishNumbers(num).join(' ').replace(/- /g, '-');
            }
        }
        return result;
    }

    let result = validateInput(num);

    return result.message;
};

const parameters = process.argv.slice(2);
console.log(numberToWords(parameters[0], parameters[1] === 'usa'));

module.exports = numberToWords;