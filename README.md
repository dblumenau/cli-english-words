# Instructions
## To run this program use node
````
node index.js 19234
````
Will produce
>nineteen thousand two hundred and thirty-four

It supports an optional additional flag, 'usa' for converting to American english numbers, which don't use 'and', and don't use a hyphen between numbers like twenty-four.
````
node index.js 15443 usa
````
Will produce
>fifteen thousand four hundred forty three

It will also validate against numbers less than zero and greater than 100000

I have included tests using the JavaScript testing framework Jest. To run these tests, do an npm install and then run 
````
npm test
````