const numberToWords = require(".");

test('1000 should output "one thousand"', () => {
  expect(numberToWords(1000)).toMatch('one thousand');
});

test('101 should output "one hundred and one"', () => {
  expect(numberToWords(101)).toMatch('one hundred and one');
});

test('352 should output "three hundred and fifty-two"', () => {
  expect(numberToWords(352)).toMatch('three hundred and fifty-two');
});

test('99843 should output "ninety-nine thousand eight hundred and forty-three"', () => {
  expect(numberToWords(99843)).toMatch('ninety-nine thousand eight hundred and forty-three');
});

test('99843 with USA flag should output "ninety nine thousand eight hundred forty three"', () => {
  expect(numberToWords(99843, true)).toMatch('ninety nine thousand eight hundred forty three');
});

test('0 should output "zero"', () => {
  expect(numberToWords(0)).toMatch('zero');
});

test('50017 should output "fifty thousand and seventeen"', () => {
  expect(numberToWords(50017)).toMatch('fifty-thousand and seventeen');
});

test('4567 should output "four thousand five hundred and sixty-seven"', () => {
  expect(numberToWords(4567)).toMatch('four thousand five hundred and sixty-seven');
});

test('10 should output "ten"', () => {
  expect(numberToWords(10)).toMatch('ten');
});


test('15 should output "fifteen"', () => {
  expect(numberToWords(15)).toMatch('fifteen');
});

test('inputting something that cannot be converted to an integer should output an error"', () => {
  expect(numberToWords('a5')).toMatch('The input could not be converted to a valid integer.');
});

test('inputting a negative number should output an error"', () => {
  expect(numberToWords('-1')).toMatch('The input must be a number between 0 and 100000.');
});

test('inputting a number larger than 1000000 should output an error"', () => {
  expect(numberToWords(1000000)).toMatch('The input must be a number between 0 and 100000.');
});